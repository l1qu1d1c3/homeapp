/**
 * Device.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const uuid = require('node-uuid');
module.exports = {

  attributes: {
    
    name: {
      type: 'string',
      defaultsTo: ''
    },

    ip: {
      type: 'string',
      defaultsTo: ''
    },

    port: {
      type: 'number',
      defaultsTo: 80
    },

    owner: {
      type: 'string'
    }
  },

};
