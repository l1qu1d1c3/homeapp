/**
 * Custom messages
 * (sails.config.messages)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.messages = {
    error: {
        badPw: 'The passwords did not pass the validation'
    }
  
  };
  