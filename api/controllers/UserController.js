/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const bcrypt = require('bcrypt');
const salt = 10;
const uuid = require('node-uuid');
module.exports = {

  /**
   * @method loadLogin
   * @desc loads login view
   */

  loadLogin: async (req, res) => {
    sails.log.debug('UserController.loadLogin started');
    if (!req.session.user) {
      return res.view('pages/login')
    } else {
      req.session.path = '/home'
      return res.view('pages/home')
    }
  },

  /**
   * @method login
   * @desc logs user in
   */

  login: async (req, res) => {
    sails.log.debug('UserController.login - Request Received');
    const data = req.allParams()
    const user = await User
      .findOne().where({
        login: data.login
      })
      .catch(error => res.serverError(error))
    if (user && Object.keys(user).length > 0) {
      const password = data.password;
      const passwordCorrect = bcrypt.compareSync(password, user.password)
      if (user.validated) {
        // User not validated
        if (passwordCorrect) {
          req.session.user = user
          req.session.isLoggedIn = true
          if (req.session.redirectURL && req.session.redirectURL !== '') {
            return res.redirect(req.session.redirectURL);
          } else {

          }
          return res.redirect('/');
        } else {
          // Password incorrect
          const toReturn = {
            data: 'notExists'
          }
          req.session.other = toReturn;
          return res.redirect('/')
        }
      } else {
        const toReturn = {
          data: 'notValidated'
        }
        req.session.other = toReturn;
        return res.redirect('/')
      }
    } else {
      const toReturn = {
        data: 'notExists'
      }
      req.session.other = toReturn;
      return res.redirect('/')
    }
  },

  /**
   * @method logOut
   * @desc logs user out
   */

  logOut: async (req, res) => {
    sails.log.debug('UserController.logOut - User ' + req.session.user.login + ' logging out');
    req.session.destroy();
    return res.redirect('/');
  },

  /**
   * @method loadRegister
   * @desc loads register view
   */

  loadRegister: async (req, res) => {
    sails.log.debug('UserController.loadRegister - Loading...');
    return res.view('pages/register')
  },

  /**
   * @method register
   * @desc creates user in db
   */

  register: async (req, res) => {
    const body = req.allParams();
    if (body.password !== body.repeatPassword) {
      sails.log.error('UserController.register - Password validation was changed from front-end')
      const toReturn = {
        error: 'wrongpw',
        message: sails.config.messages.error.badPw
      }
      return res.redirect('/register')
        .json({
          toReturn
        })
    } else {
      // Delete the repeated password
      delete body.repeatPassword
      // Check if user exists
      const userExists = await User.find({
        or: [{
          login: body.login
        }, {
          email: body.login
        }]
      }).catch(error => res.serverError(error))
      if (userExists.length === 0) {
        // if user doesnt exist, save it, redirect to home page and set the session to notify that the email validation has to be done
        const genSalt = await bcrypt.genSalt(salt).catch(error => sails.log.error('UserController.register - Error generating a salt ' + error))
        const hash = bcrypt.hashSync(body.password, genSalt);
        body.password = hash
        body.validationKey = uuid.v4();
        const user = await User.create(body).fetch()
          .catch(error => res.serverError(error))
        sails.log.debug('UserController.register - User created');
        const toReturn = {
          data: 'registered'
        }
        // TODO: SEND VALIDATION EMAIL TO VERIFY ACCOUNT WITH validationKey in it.
        req.session.other = toReturn;
        return res.redirect('/');
      } else {
        // if user exists, send the data back to the frontend to set it in the form again
        req.session.other = {
          data: body
        }
        return res.redirect('/register');
      }
    }
  },

  /**
   * @method validateUser
   * @desc validates user
   * @param {string} id
   */

  validateUser: function (req, res) {

  },

  /**
   * @method loadProfile
   * @desc loads user profile
   */

  loadProfile: async (req, res) => {
    sails.log.debug('UserController.loadProfile - Request received by ' + req.session.user.login)
    if (req.session.user) {
      const body = req.session.user
      const user = await User.findOne({
        login: body.login
      }).catch((error) => {
        sails.log.error('UserController.loadProfile - ' + error)
        return res.redirect('/')
      })
      if (user) {
        return res.view('pages/user/profile', {
          user: user
        });
      } else {
        sails.log.error('UserController.loadProfile - No user found')
        return res.redirect('/')
      }
    } else {
      sails.log.error('UserController.loadProfile - Request received by' + req.session.user.login);
      return res.redirect('/')
    }
  },

  /**
   * @method updatePassword
   * @desc updates user password
   */

  updatePassword: async (req, res) => {
    sails.log.debug('UserController.updatePassword - Request received by ' + req.session.user.login);
    if (req.body) {
      const body = req.body
      if (req.body && req.body.new !== '' && req.body.old !== '' && req.body.repeat !== '') {
        const user = await User.findOne({
          login: req.session.user.login
        }).catch((error) => {
          sails.log.error('UserController.updatePassword - Error finding user ' + error)
          return res.redirect('/');
        })
        if (user) {
          const oldPw = body.old;
          const newPass = body.new;
          const repeat = body.repeat;
          const passwordCorrect = bcrypt.compareSync(oldPw, user.password)
          if (!passwordCorrect) {
            sails.log.error('UserController.updatePassword - Passwords provided does not match');
            return res.serverError()
          } else {
            // Repeated password validation
            if (newPass === repeat) {
              const genSalt = await bcrypt.genSalt(salt).catch(error => sails.log.error('UserController.register - Error generating a salt ' + error))
              const hash = bcrypt.hashSync(newPass, genSalt);
              user.password = hash;
              const userUpdated = await User.update({
                login: user.login
              }, user).fetch().catch((error) => {
                sails.log.error('UserController.updatePassword - Error updating user password ' + error)
                return res.serverError()
              })
              if (userUpdated) {
                delete req.session.user
                req.session.isLoggedIn = false
                const toReturn = { data: 'passChanged'}
                req.session.other = {}
                req.session.other = toReturn
                return res.redirect('/login');
              }
            } else {
              //Repeated passwords do not match
              sails.log.error('UserController.updatePassword - Passwords provided does not match');
              return res.serverError()
            }
          }
        } else {
          sails.log.error('UserController.updatePassword - No user found')
          return res.serverError()
        }
      } else {
        sails.log.error('UserController.updatePassword - No values provided by ' + req.session.user.login)
        return res.serverError();
      }
    } else {
      sails.log.error('UserController.updatePassword - Error thrown by ' + req.session.user.login)
      return res.serverError();
    }
  },

  /**
   * @method updateImage
   * @desc updates user avatar
   */
  updateImage: async (req, res) => {
    sails.log.debug('UserController.updateImage - Request received by ' + req.session.user.login);
    if (req) {
      const image = req.body.data
      const user = req.session.user.id
      const userData = await User.findOne({
        id: user
      }).catch((error) => {
        sails.log.error('UserController.updateImage - Error fetching user -' + error)
        return res.serverError();
      });
      if (userData) {
        userData.avatar = image
        User.update({
          id: userData.id
        }, userData).catch((error) => {
          sails.log.error('UserController.updateImage - Error updating user avatar -' + error)
        });
        sails.log.debug('UserController.updateImage - Done');
        userData.id = userData._id
        delete userData._id
        req.session.user = userData
        return res.ok()
      } else {
        sails.log.error('UserController.updateImage - No user found')
        return res.serverError();
      }
    } else {
      sails.log.error('UserController.updateImage - No data received')
      return res.serverError()
    }
  }
};
