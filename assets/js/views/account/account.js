$(document).ready(() => {
  // Avatar uploader
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
        $('#imagePreview').hide();
        $.post('/updateImage', {
            data: e.target.result
          }).done(() => {
            $('#page-wrapper').append(`<div class="alert alert-success sm" id="pwChanged" role="alert" style="width:250px;display: block;position:absolute; left: 50%; top: 0">
            Profile image updated <i class="fa fa-check"></i>
        </div>`);
          setTimeout(() => {
            $('div#pwChanged').remove();
          }, 3 * 1000)
          })
          .fail((error) => {

          })
        $('#imagePreview').fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imageUpload").change(function () {
    readURL(this);
  });
  // Avatar uploader end


  $('a#changePassword').click(() => {
    $('#oldPassword').val('')
    $('#newPassword').val('')
    $('div.modal small').hide();
    $('#repeatNewPassword').val('')
    $('div#changePassword').modal('show');
    $('input[type=password]').css('border-color', '#ccc');
  })

  $('button#changePassword').click(() => {
    var oldPw = $('#oldPassword').val()
    var newPw = $('#newPassword').val()
    var rNewPw = $('#repeatNewPassword').val()
    if (newPw !== rNewPw || (newPw === '' || rNewPw === '' || oldPw === '')) {
      $('#wrongPW1, #wrongPW2').show()
      $('#newPassword').css('border-color', 'red');
      $('#repeatNewPassword').css('border-color', 'red');
      return false
    } else if (oldPw === newPw || oldPw === rNewPw) {
      $('#reusePW').show()
      $('#oldPassword').css('border-color', 'red');
      $('#newPassword').css('border-color', 'red');
      $('#repeatNewPassword').css('border-color', 'red');
    } else {
      $.post('/updatePassword', {
          old: oldPw,
          new: newPw,
          repeat: rNewPw
        }).done(() => {
          document.location.reload()
        })
        .fail(() => {
          $('div#changePassword').modal('hide');
          $('#page-wrapper').append(`<div class="alert alert-danger sm" id="pwChanged" role="alert" style="width:350px;display: block;position:absolute; left: 50%; top: 0">
            Something went wrong, please try again later <i class="fa fa-times"></i>
        </div>`);
          setTimeout(() => {
            $('div#pwChanged').remove();
          }, 3 * 1000)
        })
    }
  })
})
