/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function (req, res, next) {

    // User is allowed, proceed to the next policy, 
    // or if this is the last policy, the controller
    if (req.session.isLoggedIn) {
      return next();
    } else {
      // User is not allowed
      // (default res.forbidden() behavior can be overridden in `config/403.js`)
      //return res.forbidden('You are not permitted to perform this action.');
  
      // save the url if the request is a GET (for redirecting)
      if (req.method === 'GET') {
        sails.log.debug("Saving GET request URL for redirecting: " + req.url);
        req.session.redirectURL = req.url;
      }
  
      // redirect to ask for credentials
      return res.redirect("/");
    }
  
  };
  