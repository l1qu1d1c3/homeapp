(function () {
  $(document).ready(() => {
    $('form').submit(() => {
      const password = $('#password').val();
      const rPassword = $('#repeatPassword').val();
      if (password !== rPassword) {
        $('#wrongPW1, #wrongPW2').show()
        $('#password').css('border-color', 'red');
        $('#repeatPassword').css('border-color', 'red');
        return false
      }
      return true;
    })
  })
})()