# HomeApp

This project is intended to manage Raspberry devices that you have at home - allowing you to execute commands remotely without having to access the device itself.
## Features

## Installation

### Prerequisites

To get started, you need Node.js. It's also recommend to have Sails.js globally installed. If you don't want to have Sails.js globally installed just use `npm run dev`. Finally, install the Node.js modules.

#### Get Node.js

```bash
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
$ sudo apt-get install -y nodejs
```

#### Get Sails.js (optional)

```bash
$ sudo npm install sails -g
```

## Usage

## Essential components used

The following components are used in this project. There are plenty more, though, check the `package.json` files.

### [Sails.js](https://github.com/balderdashy/sails)

This is the backend and data provider.

### Bootstrap 4

###


## Author

Alejandro Fernandez

## License

MIT
